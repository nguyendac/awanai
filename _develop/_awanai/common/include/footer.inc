<div class="row bx_footer">
  <h2 class="logo_ft">
    <a href=""><img src="./common/images/logo.png" alt=""></a>
  </h2>
  <!--/logo_footer-->
  <ul class="nav_ft">
    <li><a href="https://awanai.com/kiyaku.pdf" target="_blank">利用規約</a></li>
    <li><a href="http://osen.co.jp/privacypolicy/" target="_blank">プライバシーポリシー</a></li>
    <li><a href="http://osen.co.jp/company/" target="_blank">運営会社</a></li>
  </ul>
  <!--/.nav_ft-->
  <p class="copyright">@ Copyright 2018 Osen Inc</p>
</div>